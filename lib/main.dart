import 'package:flutter/material.dart';
import 'package:primeros_pasos/utils/random_words.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ejemplo',
      theme: ThemeData(
        appBarTheme:  const AppBarTheme(
          backgroundColor: Colors.white,
          foregroundColor: Colors.black
        )
      ),
      home: RandomWords(),
    );
  }
}
